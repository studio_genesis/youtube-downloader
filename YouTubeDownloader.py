### SETTINGS ###
sleep_timer = 5

### CHECK DEPENDENCIES ###
from importlib import util
from os import system
from Modules import errors

if util.find_spec("pytube") is None:
    try:
        system('pip3 install pytube')
    except:
        system('pip install pytube')

### LOAD DEPENDENCIES ###
from time import sleep
from pytube import YouTube, exceptions
from sys import argv

### SCRIPT ###
def Start():
    try:
        GetDownloadList(argv[1]) 
    except IndexError:
        errors.NoArgumentsGiven()
    

def GetDownloadList(path):
    download_list = []
    try:
        f = open(path, "r")
        for line in f:
            download_list.append(str(line).rstrip())

        DownloadList(download_list)
    
    except FileNotFoundError:
        errors.FileNotFound()
        

def DownloadList(videos):
    if len(videos) > 0:
        for video in videos:
            result = None
            while result is None:
                try:
                    yt = YouTube(video)
                    yt = yt.streams.filter(progressive=True, file_extension='mp4')
                    yt = yt.order_by('resolution')
                    yt = yt.desc()
                    yt = yt.first()
                    print("Downloading: " + str(video))
                    result = yt.download()

                except exceptions.VideoUnavailable: # This can be broken down further
                    errors.VideoUnavailable(sleep_timer, video)
                    sleep(sleep_timer)                                 
    else:
        errors.FileEmpty()


Start()