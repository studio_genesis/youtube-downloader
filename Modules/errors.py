def NoArgumentsGiven():
    response = "No filepath given!"
    response += "\n\n"
    response += "Usage: python3 YouTubeDownloader.py [FILE]"
    response += "\n"

    print(response)

def FileNotFound():
    response = "File not found! Please check File Path and try again."
    response += "\n\n"
    response += "Usage: python3 YouTubeDownloader.py [FILE]"
    response += "\n"

    print(response)
    
def FileEmpty():
    response = "File given is empty! Please check your provided file has at least 1 YouTube link!"
    response += "\n\n"
    response += "Usage: python3 YouTubeDownloader.py [FILE]"
    response += "\n"

    print(response)

def VideoUnavailable(sleep_timer, video):
    response = "Video currently unavailable! Trying again in " + str(sleep_timer) + " seconds..."
    response += "\n"
    response += "If this continues please check the link provided: " + str(video)
    response += "\n"

    print(response)