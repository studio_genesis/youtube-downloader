Usage:<br />
    Create a file with each line being an individual YouTube video link. Then run the following commmand:<br />
        python3 YouTubeDownloader.py [FILE]<br />

TODO:<br />
    - Add downloading of playlists<br />
    - Make Windows friendly<br />
    - Add verbose loggings<br />
    - Add adjustable sleep timer<br />
    - Add parameter to set quality<br />